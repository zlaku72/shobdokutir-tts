DOCKER_IMAGE = gcr.io/shobdokutir-web/shobdokutir-tts
GIT_COMMIT = local.$(shell git rev-parse --short HEAD)$(shell git diff-index --quiet HEAD || echo .dirty)
DOCKER_IMAGE_TAG = $(DOCKER_IMAGE):$(GIT_COMMIT)

format:
	tox -e format

install-dev:
	pip install -r requirements-dev.txt

test:
	tox
	pytest

docker-build:
	docker build -t $(DOCKER_IMAGE_TAG) -f docker/Dockerfile .

docker-shell:
	docker run -ti --gpus all $(DOCKER_IMAGE_TAG) /bin/bash

clean: clean-build clean-pyc clean-test ## remove all build, test, coverage and Python artifacts

clean-build: ## remove build artifacts
	rm -fr build/
	rm -fr dist/
	rm -fr .eggs/
	rm -rf scripts/shobdokutir-tts/kimvut.bitbucket.io
	find . -name '*.egg-info' -exec rm -fr {} +
	find . -name '*.egg' -exec rm -f {} +

clean-pyc: ## remove Python file artifacts
	find . -name '*.pyc' -exec rm -f {} +
	find . -name '*.pyo' -exec rm -f {} +
	find . -name '*~' -exec rm -f {} +
	find . -name '__pycache__' -exec rm -fr {} +

clean-test: ## remove test and coverage artifacts
	rm -fr .tox/
	rm -fr .coverage
	rm -rf cobertura/
	rm -fr htmlcov/
	rm -fr .pytest_cache
