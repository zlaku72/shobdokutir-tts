"""This is a cookiecutter example - feel free to delete this module."""

import pytest

from shobdokutir_tts.main import Foo


@pytest.fixture
def foo_instance() -> Foo:
    """Fixture returning an instance of Foo.

    Fixtures is a powerful concept, read more about them in the Pytest docs.
    """
    return Foo("param1", "param2")


def test_return_state(foo_instance: Foo) -> None:
    """Good tests define expected test behavior in docstrings."""
    expected = "Current state is: attr1: param1 attr2: param2"
    actual = foo_instance.return_state()

    # stick to an order of expected vs actual so tests are logical to read
    assert expected == actual
