# Screening Problems
Here are all the instructions for the screening. Please make sure to follow the steps as instructed 
because the first qualification in this screening process is the skill of "following instructions". 
In case the instruction is vague or ambiguous, please use your best judgment. You should heavily utilize the docstrings 
to rationalize your choices/judgments.

The instructions assume that you are using an Ubuntu or a Mac (OSX) computer. Use of the Windows operating system is
specifically discouraged.

## Generic Instructions
In order to participate in the volunteer screening,
1. Clone the git repository [Shobdokutir TTS](https://gitlab.com/shobdokutir-public-projects/shobdokutir-tts)
2. Read the README and make sure you can successfully run the commands `make install-dev`, `make format`, `make clean` 
   and `make test` from the folder root.
4. Follow the instructions in the README to authenticate and access your Google Cloud platform account.
5. Solve as many problems as you can from any one of the three sections below. You can also try to solve more than one 
   section if you like. 
4. To submit your solution,
    - Create a new conda environment, run `make install-dev` and make sure all your code runs in that environment
    - Run `make clean`
    - Zip while excluding the unwanted folders:
      `zip -r shobdokutir-tts.zip . -x "*.git*" "*.DS_Store" "*.idea*" "*resources*"` 
    - Upload `shobdokutir-tts.zip` using [this form](https://forms.gle/7cVYUeMPtmkYf5ZV7)
    - You can make as many submissions as you want, but only the single latest submission will be evaluated 
      (older submissions may get deleted)

_Please make sure to use the same email address that you used to sign up for the volunteership; otherwise, 
your submission will be lost._

## Section 1: Data Processing Problems
**Deadline: July 8th, 2023**

### Problem 1: Audio Encoding
1. You have been given the read access to the GCS bucket `shobdokutir_tts_datasets`. Here is how to check your access:
   - Log in to your [Google cloud console](https://console.cloud.google.com/)
   - Click on the button `Select a Project` and search and select the project `shobdokutir-web`.
   - Click the hamburger icon in the top left corner and navigate to `Could storage >> Buckets`. You should be able to 
   see a bucket named `shobdokutir_tts_datasets`.
   - Please check the README to authenticate for cli/python access to GCP. 
2. (practice) Try loading a single audio (`mp3`) file from the `shobdokutir_tts_datasets` bucket to python directly.
3. (practice) Trim the audio file using python so that there is no silence to the start and end of the audio.
4. (practice) Save the trimmed audio file as a mono channel `.wav` file with a sampling rate of 24kHz.
5. At this point we'll use the encodec library from facebook research (https://github.com/facebookresearch/encodec) 
   to encode the audio using some sequences of "codes". 
	- Finish implementing the `AudioEncodec` class written in the `shobdokutir_tts.data.preprocess` module
	- To ensure that the `AudioEncodec` code works without internet connection, use the encodec model checkpoint 
   (`encodec_24khz-d7cc33bc.th`) provided in the `resources` folder.
    - Ensure that the audio plays exactly like the original when encoded using `AudioEncodec.encode_audio` and then 
   decoded back using `AudioEncodec.decode_audio`

**Hint**: You can solve the practice steps (2 to 4) using the following code. Please check the implementation of the
`load_and_trim_audio` function to understand how to load, process, and save audio files in python directly from GCS.
```python
from shobdokutir_tts.data.vggvox.utils import load_and_trim_audio

load_and_trim_audio("gs://shobdokutir_tts_datasets/cv-corpus-13-delta/bn/clips/common_voice_bn_36539491.mp3",
                    24000,
                    "~/test.wav")
```

### Problem 2: Computing Speaker Embeddings

In this problem, we'll compute the "speaker embeddings" corresponding to the voices in the audio files. Speaker 
embeddings for this problem are 512 dimensional vectors that represents the vocal characteristics of a speaker. The
cosine similarity of two voice embeddings extracted from the voice of the same speaker should approach towards 1.0, 
while the embeddings extracted from two different speakers' voice will approach towards 0.5 or lower. 

1. Use the `SpeakerEmbeddingExtractor` (In `shobdokutir_tts.data.preprocess` module) class to compute the speaker 
   embeddings corresponding to an audio file.
2. Finish implementing the `compute_voice_similarity` function. 
3. Use the [sklearn.manifold.TSNE](https://scikit-learn.org/stable/modules/generated/sklearn.manifold.TSNE.html) 
   function in `scikit-learn` to finish implementing the `compute_tsne` function. Please make sure that the plot is 
   aesthetically pleasing. Extra points will be given for an interactive plotting (e.g. using `plotly`).

### Problem 3: Tokenizing the Bangla Text Transcripts
In this problem, we'll tokenize the Bangla text transcripts of the audio. Before starting, you may like to read an 
[overview of tokenizer](https://huggingface.co/learn/nlp-course/chapter2/4?fw=pt), and would like
to get an introduction to [gpt2 tokenizer](https://huggingface.co/docs/transformers/model_doc/gpt2) from huggingface.

1. Finish implementing `BanglaTokenizer` in `shobdokutir_tts.data.preprocess`.
2. Extract the token indices corresponding to this sentence "এটা একটা পরীক্ষামূলক পর্যালোচনা" using the method `BanglaTokenizer.tokenize`
3. Use the method `BanglaTokenizer.detokenize` to convert the list of token indices back to the Bangla sentence.

[//]: # (### Problem 4: Creating Webdataset)

[//]: # ()
[//]: # ()
[//]: # (### Problem 5: Implementing a dataflow pipeline )


## Section 2: Model Building Problems
Coming Soon

## Section 3: GUI Building Problems
Coming Soon
