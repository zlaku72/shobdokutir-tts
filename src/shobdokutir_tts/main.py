import argparse
import re
from typing import Any, Optional

import yaml


class KimvutObject:
    """
    Kimvut command lines are recommended to adhere to a central configuration file.
    The structure of the configuration can be hierarchical as mentioned in the
    point # 1 of `https://itanveer.com/coding-style`.
    """

    @staticmethod
    def camel_2_snake(camel_input: str) -> str:
        return re.sub(r"(?<!^)(?=[A-Z])", "_", camel_input).lower()

    @classmethod
    def from_yaml(cls, yaml_filename: str) -> Any:
        conf = yaml.load(open(yaml_filename, "rb"), Loader=yaml.FullLoader)
        if conf["name"] == KimvutObject.camel_2_snake(cls.__name__):
            class_configs = {key: conf[key] for key in conf if key != "name"}
            return cls(**class_configs)
        else:
            raise Exception("KimvutObject: Class configuration not recognized")


class Foo(KimvutObject):
    """This is class-level documentation. This is mandatory for every class

    Class level variables are generally discouraged.
    """

    def __init__(self, param1: str, param2: Optional[int]) -> None:
        """Documentation of every method and arguments is highly recommended.

        Args:
            param1: Description of `param1`. Types are not needed as they are
                specified with the type annotations.
            param2: Description of `param2`.
                    This can be multiple lines.
        """
        self.attr1 = param1
        self.attr2 = param2

    def return_state(self) -> str:
        """A simple method used for example tests."""

        return f"Current state is: attr1: {self.attr1} attr2: {self.attr2}"


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--conf", default="confs/main_config.yaml")
    args = parser.parse_args()
    foo = Foo.from_yaml(args.conf)
    print(foo.return_state())
