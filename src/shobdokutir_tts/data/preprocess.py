from typing import BinaryIO, List

import numpy as np

from shobdokutir_tts.data.vggvox.constants import model_params, signal_params
from shobdokutir_tts.data.vggvox.utils import get_model, load_audio_spectrogram


class AudioEncodec:
    def __init__(self):
        """
        The encodec (https://github.com/facebookresearch/encodec) model should be loaded here.
        The model `encodec_model_24khz` will be loaded and the bandwidth will be set to 3 kbps (n_q = 4).
        """
        raise NotImplementedError

    def encode_audio(self, raw_audio: BinaryIO) -> np.ndarray:
        """
        Given a raw audio of sampling rate 24kHz, the encodec codes will be returned.
        """
        raise NotImplementedError

    def decode_audio(self, encoded_audio: np.ndarray) -> np.ndarray:
        """
        Given the encodec codes, the raw audio is returned
        """
        raise NotImplementedError


class SpeakerEmbeddingExtractor:
    """
    Extracts speaker embedding from a short audio file
    """

    def __init__(self, model_path="resources/vggvox_model.h5"):
        self.vggvox = get_model(
            signal_params["dim"],
            signal_params["n_classes"],
            resume_from="resources/vggvox_model.h5",
            params=model_params,
        )

    def __call__(self, audio_filename: str) -> np.ndarray:
        """
        Given a raw audio, the speaker embedding is returned assuming it contains only one voice.
        The `audio_filename` can be a local audio file or a file located in a Google Cloud Storage (GCS) bucket.
        If the audio is located at GCS, the filename must be started with "gs://".
        """
        audio_spectrogram = load_audio_spectrogram(audio_filename)
        speaker_embedding = self.vggvox.predict(audio_spectrogram)
        return speaker_embedding


def compute_voice_similarity(audio_filename_1: str, audio_filename_2: str) -> float:
    """
    Given two audio files, returns the cosine similarity of the two speakers.
    The audio files can be stored either in local filesystem or in GCS. If stored in GCS, the filename must start
    with "gs://".
    """
    raise NotImplementedError


def compute_tsne(audio_folder: str, tsv_path: str, output_filename: str) -> None:
    """
    Given the locations of the audio files, and the corresponding tsv file path, this function will create a tsne
    plot and stores in an output png file. The datapoints having the same `client_id` in the tsv file should be
    marked by the same color.
    Both the input audio files and the tsv files can be located in either a local file system or in gcs.
    If located in gcs, the file or folder names will be prefixed with "gs://".
    """
    raise NotImplementedError


class BanglaTokenizer:
    def __init__(self):
        """
        The huggingface gpt2 tokenizer will be loaded here.
        The tsv files will also be read and preloaded into a dictionary here, so that given an audio filename, it
        becomes O(1) to find the text transcript. The tsv files are located here:
        gs://shobdokutir_tts_datasets/cv-corpus-13-delta/bn
        """
        raise NotImplementedError

    def get_corresponding_text(self, audio_filename: str) -> str:
        """
        Given an audio filename (just the filename. no path or extension), returns the text transcript from the
        tsv file. It must be very fast.
        """
        raise NotImplementedError

    def tokenize(self, text: str) -> List[int]:
        """
        Given a Bangla text in unicode, this method will return the token indices using the gpt2 tokenizer
        """
        raise NotImplementedError

    def detokenize(self, token_ids: List[int]) -> str:
        """
        Given a list of token indices from the gpt2 tokenizer, return the Bangla text in unicode.
        """
        raise NotImplementedError
