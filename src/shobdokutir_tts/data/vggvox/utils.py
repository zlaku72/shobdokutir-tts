import os

import librosa
import numpy as np
import tensorflow as tf
from pedalboard_native.io import AudioFile

import shobdokutir_tts.data.vggvox.model as model


def resample_and_load(audio_filename, desired_sampling_rate):
    # Wav format and sampling rate ensured (fast)
    wav_data = load_trim_return_audio(audio_filename, desired_sampling_rate).astype(
        "float32"
    )
    # Adjust amplitude/format
    signal_mean = wav_data.mean()
    signal_std = wav_data.std()
    wav_data = (wav_data - signal_mean) / signal_std
    return wav_data, desired_sampling_rate


def lin_spectrogram_from_wav(wav, hop_length, win_length, n_fft=1024):
    linear = librosa.stft(
        wav, n_fft=n_fft, win_length=win_length, hop_length=hop_length
    )  # linear spectrogram
    return linear.T


def load_audio_spectrogram(
    path,
    win_length=400,
    sr=16000,
    hop_length=160,
    n_fft=512,
    spec_len=250,
    mode="eval",
):
    """
    Loads an audio file and computes the specto
    """
    wav = load_wav(path, sr=sr, mode=mode)
    spectrogram = compute_spectrogram(
        wav, win_length, hop_length, n_fft, spec_len, mode
    )
    return np.expand_dims(np.array(spectrogram), axis=0)


def get_model(input_dims, n_classes, resume_from=None, params=None):
    network_eval = model.vggvox_resnet2d_icassp(
        input_dim=input_dims, num_class=n_classes, mode="eval", args=params
    )
    if resume_from and os.path.isfile(resume_from):
        network_eval.load_weights(os.path.join(resume_from), by_name=True)
    else:
        raise IOError("==> No pretrained model found!")

    return network_eval


def compute_spectrogram(
    audio, win_length=400, hop_length=160, n_fft=512, spec_len=250, mode="train"
):
    linear_spect = lin_spectrogram_from_wav(audio, hop_length, win_length, n_fft)
    mag, _ = librosa.magphase(linear_spect)  # magnitude
    mag_T = mag.T
    freq, time = mag_T.shape
    if mode == "train":
        if time > spec_len:
            randtime = np.random.randint(0, time - spec_len)
            spec_mag = mag_T[:, randtime : randtime + spec_len]
        else:
            spec_mag = np.pad(mag_T, ((0, 0), (0, spec_len - time)), "constant")
    else:
        spec_mag = mag_T
    # preprocessing, subtract mean, divided by time-wise var
    mu = np.mean(spec_mag, 0, keepdims=True)
    std = np.std(spec_mag, 0, keepdims=True)
    return (spec_mag - mu) / (std + 1e-5)


def load_wav(audio_filename, sr, mode="train"):
    """
    Loads an audio file and returns the audio samples as a numpy array
    """
    try:
        wav, sr_ret = resample_and_load(audio_filename, sr)
    except Exception as e:
        raise Exception(f"Exception loading wav file {e}")

    assert sr_ret == sr

    if mode == "train":
        extended_wav = np.append(wav, wav)
        if np.random.random() < 0.3:
            extended_wav = extended_wav[::-1]
        return extended_wav
    elif mode == "unchanged":
        return wav
    else:
        extended_wav = np.append(wav, wav[::-1])
        return extended_wav


def load_and_trim_audio(
    src_path: str, tgt_sample_rate: int, tgt_path: str, top_db: int = 20
) -> None:
    """
    This function
    (1) Reads a short audio file,
    (2) Resamples to make the sample rate equal to `tgt_sample_rate`
    (3) Makes the audio mono channel
    (4) Removes silence from the start and the end (Trim)
    (5) Stores the audio as a local audio file.
    This method can read `mp3`, `wav` and several other major audio file formats.
    src_path: Full path of the source file. Source can be stored in the local filesystem or in the Google Cloud
              Storage (GCS). If stored at GCS, the path string must start with "gs://"
    tgt_path: Full path of the target audio file.
    top_db: the decibel below which the audio is considered to be silence
    """
    with AudioFile(
        tgt_path, "w", samplerate=tgt_sample_rate, num_channels=1
    ) as f_write:
        f_write.write(load_trim_return_audio(src_path, tgt_sample_rate, top_db))


def load_trim_return_audio(
    src_path: str, tgt_sample_rate: int, top_db: int = 20
) -> np.ndarray:
    """
    This function
    (1) Reads a short audio file,
    (2) Resamples to make the sample rate equal to `tgt_sample_rate`
    (3) Makes the audio mono channel
    (4) Removes silence from the start and the end (Trim)
    (5) Returns the audio content
    This method can read `mp3`, `wav` and several other major audio file formats.
    src_path: Full path of the source file. Source can be stored in the local filesystem or in the Google Cloud
              Storage (GCS). If stored at GCS, the path string must start with "gs://"
    tgt_path: Full path of the target audio file.
    top_db: the decibel below which the audio is considered to be silence
    ---
    Returns: A numpy array containing the trimmed version of the raw audio
    """
    with tf.io.gfile.GFile(src_path, "rb") as source_audio:
        with AudioFile(source_audio).resampled_to(tgt_sample_rate) as f:
            mono_audio = librosa.to_mono(f.read(f.frames))
            audio_trimmed, _ = librosa.effects.trim(mono_audio, top_db=top_db)
            return audio_trimmed
