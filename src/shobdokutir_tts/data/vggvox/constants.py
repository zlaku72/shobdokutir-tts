model_params = {
    "net": "resnet34s",  # choices = ['resnet34s', 'resnet34l']
    "ghost_cluster": 2,
    "vlad_cluster": 8,
    "bottleneck_dim": 512,
    "aggregation_mode": "gvlad",  # choices = ['avg', 'vlad', 'gvlad']
    "loss": "softmax",  # choices = ['softmax', 'amsoftmax']
    "test_type": "normal",  # choices = ['normal', 'hard', 'extend']
}
signal_params = {
    "dim": [257, None, 1],
    "nfft": 512,
    "spec_len": 250,
    "win_length": 400,
    "hop_length": 160,
    "n_classes": 5994,
    "sampling_rate": 16000,
    "normalize": True,
}
