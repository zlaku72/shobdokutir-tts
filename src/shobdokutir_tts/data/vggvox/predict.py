import argparse
import os

import numpy as np
import tqdm

import shobdokutir_tts.data.preprocess
import shobdokutir_tts.data.vggvox.model as model
import shobdokutir_tts.data.vggvox.utils
import shobdokutir_tts.data.vggvox.utils as ut

params = {
    "dim": (257, None, 1),
    "nfft": 512,
    "spec_len": 250,
    "win_length": 400,
    "hop_length": 160,
    "n_classes": 5994,
    "sampling_rate": 16000,
    "normalize": True,
}


def parse_arguments():
    parser = argparse.ArgumentParser()
    # set up training configuration.
    parser.add_argument("--gpu", default="", type=str)
    parser.add_argument("--resume", default="", type=str)
    parser.add_argument("--batch_size", default=16, type=int)
    parser.add_argument(
        "--data_path", default="/media/weidi/2TB-2/datasets/voxceleb1/wav", type=str
    )
    # set up network configuration.
    parser.add_argument(
        "--net", default="resnet34s", choices=["resnet34s", "resnet34l"], type=str
    )
    parser.add_argument("--ghost_cluster", default=2, type=int)
    parser.add_argument("--vlad_cluster", default=8, type=int)
    parser.add_argument("--bottleneck_dim", default=512, type=int)
    parser.add_argument(
        "--aggregation_mode",
        default="gvlad",
        choices=["avg", "vlad", "gvlad"],
        type=str,
    )
    parser.add_argument(
        "--loss", default="softmax", choices=["softmax", "amsoftmax"], type=str
    )
    parser.add_argument(
        "--test_type", default="normal", choices=["normal", "hard", "extend"], type=str
    )
    parser.add_argument(
        "--task",
        default="compute_eer",
        choices=["compute_eer", "compute_embedding"],
        type=str,
    )
    parser.add_argument("--input_audio", default=None, type=str)
    args = parser.parse_args()
    return args


def get_model(params, args):
    network_eval = model.vggvox_resnet2d_icassp(
        input_dim=params["dim"], num_class=params["n_classes"], mode="eval", args=args
    )
    if args.resume:
        if os.path.isfile(args.resume):
            network_eval.load_weights(os.path.join(args.resume), by_name=True)
            print("==> Model successfully loaded {}.".format(args.resume))
        else:
            raise IOError("==> no checkpoint found at '{}'".format(args.resume))
    else:
        raise IOError("==> please type in the model to load")

    return network_eval


def set_result_path(args):
    model_path = os.path.abspath(args.resume)
    path_name = os.path.split(model_path)
    name_ext = os.path.splitext(path_name[-1])
    result_path = os.path.join("../result", name_ext[0])
    if not os.path.exists(result_path):
        os.makedirs(result_path)
    return result_path


def get_audio_embedding(audio, network, window_size=2, sample_rate=16000):
    """
    Given an audio data, computes the speaker embeddings for every window of size `window_size'
    """
    if type(audio) == str:
        print("==> Loading audio file {0}".format(audio))
        audio = shobdokutir_tts.data.vggvox.utils.load_wav(
            audio, sample_rate, "unchanged"
        )
    window_size = int(window_size * sample_rate)
    audio_len = len(audio)
    last_split_length = audio_len % window_size

    print("==> Splitting the audio")
    splits = []
    if last_split_length > 0:
        last_split = np.pad(
            audio[-last_split_length:],
            (0, window_size - last_split_length),
            mode="constant",
        )
        splits.append(last_split)
    if audio_len > last_split_length:
        split_size = (audio_len - last_split_length) // window_size

        if last_split_length > 0:
            prev_splits = np.split(audio[:-last_split_length], split_size)
        else:
            prev_splits = np.split(audio, split_size)
        splits = prev_splits + splits

    splits = np.array(splits)
    splits = np.concatenate((splits, splits[:, ::-1]), axis=1)
    nb_splits = splits.shape[0]
    spectograms = []

    print(
        "==> Computing spectograms for each {0} sec splits".format(
            window_size / sample_rate
        )
    )
    for i in tqdm.tqdm(range(nb_splits)):
        spectograms.append(
            ut.compute_spectrogram(
                splits[i, :],
                win_length=params["win_length"],
                hop_length=params["hop_length"],
                n_fft=params["nfft"],
                spec_len=params["spec_len"],
                mode="eval",
            )
        )

    spectograms = np.array(spectograms)
    expanded_spectograms = np.expand_dims(spectograms, -1)
    embeddings = network.predict(expanded_spectograms)
    return embeddings


def detect_speaker_change(embeddings, threshold=0.7):
    affinity_matrix = embeddings.dot(embeddings.T)
    np.save("../result/affinity_matrix.npy", affinity_matrix)
