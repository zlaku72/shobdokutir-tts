# This setup.py file is used to install necessary packages on Dataflow workers
import os
import re

import setuptools


def get_version():
    init_file = os.path.abspath("./src/shobdokutir_tts/__init__.py")
    with open(init_file) as fp:
        for line in fp:
            m = re.search(r'^\s*__version__\s*=\s*([\'"])([^\'"]+)\1\s*$', line)
            if m:
                version = m.group(2)
                return version
    raise RuntimeError("Unable to find own __version__ string")


def read_me():
    readme_file = os.path.abspath("./README.md")
    with open(readme_file) as fp:
        return fp.read()
    raise RuntimeError("Unable to find README.md at root folder")


setuptools.setup(
    name="shobdokutir_tts",
    version=get_version(),
    description="A Transformer-based Bangla Text To Speech",
    long_description=read_me(),
    long_description_content_type="text/markdown",
    author="Md Iftekhar Tanveer",
    author_email="md.iftekhar.tanveer@gmail.com",
    url="https://gitlab.com/shobdokutir-public-projects/shobdokutir-tts",
    classifiers=[
        "Development Status :: 4 - Beta",
        "Programming Language :: Python",
        "Programming Language :: Python :: 3",
        "Programming Language :: Python :: 3.10",
        "Natural Language :: English",
        "Operating System :: POSIX :: Linux",
        "Operating System :: MacOS :: MacOS X",
        "Programming Language :: Python :: Implementation :: CPython",
    ],
    python_requires=">=3.10",
    # Soft pinning is recommended, unless absolutely necessary
    install_requires=[
        "PyYAML==5.4b2",
        "tensorflow",
        "pedalboard",
        "librosa",
        "encodec",
        "tqdm",
    ],
    packages=setuptools.find_packages(where="src", exclude=["tests"]),
    package_dir={"": "src"},
)
