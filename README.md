# shobdokutir-tts

A Transformer-based Bangla Text To Speech

## Install for development

1. Create a new [Conda environment](https://docs.conda.io/en/latest/miniconda.html) 
with the python version 3.10.
```shell
$ conda create -n env python==3.10
$ conda activate env
```
2. Run the following within the newly created environment:
```shell
(env) $ make install-dev
```
This will install development dependencies, followed by installing this package
itself in [editable](https://pip.pypa.io/en/stable/topics/local-project-installs/#editable-installs) mode.

## Accessing Google Cloud Storage (GCS)
Some portions of the library assumes default access to GCS. To ensure default authentication, please install
[gcloud](https://cloud.google.com/sdk/docs/install) and run the following command:
```shell
(env) $ gcloud auth application-default login
```
Then use your Google account to authenticate. If your email address is not from Gmail, you can create a
Google account corresponding to your email address using [this link](https://accounts.google.com/signupwithoutgmail).

Once authenticated, test your access using the following command:
```shell
(env) $ gsutil ls
```

## Auto-formatting
Tests may fail if the repository is not formatted correctly. The repository can be
auto-formatted as much as possible using the following command:

```bash
$ (env) $ make format
```

## Run tests

Tests can be invoked in two ways: `pytest` and `tox`. A shortcut for running both is as follows:

```shell
$ (env) $ make test
```

### Run tests via `pytest`

Pytest is the recommended test runner framework and coverage can measure the
test coverage. The default pytest config is defined in `pyproject.toml`.

```shell
# for all tests
(env) $ pytest tests/

# for one module of tests
(env) $ pytest tests/test_main.py

# for one specific test
(env) $ pytest tests/test_main.py::test_return_state
```

### Run tests via `tox`

Tox is a tool to automate and standardize tests and test environments. 
A test environment could be based on python versions, or could be specific to
documentation, or whatever else. See `tox.ini` for examples.

```sh
# run all environments
(env) $ tox

# run a specific environment
(env) $ tox -e py36
```

## Updating the version

Owners must bump the version themselves when ready to make a
new release.

Just specify the correct version in the `__version__` variable inside `src/shobdokutir_tts/__init__.py`
